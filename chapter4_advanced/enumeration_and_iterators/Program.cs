﻿using System;
using System.Collections.Generic;

namespace enumeration_and_iterators {
    class MainClass {
        static IEnumerable<int> Fibs(int fibCount) {
            for (int i = 0, prevFib = 1, curFib = 1; i < fibCount; i++) {
                yield return prevFib;
                int newFib = curFib + curFib;
                prevFib = curFib;
                curFib = newFib;
            }
        }

        static IEnumerable<string> Foo(bool breakEarly = false) {
            yield return "One";
            yield return "Two";

            if (breakEarly)
                yield break;

            yield return "Three";
        }

        static IEnumerable<int> EvenNumbersOnly(IEnumerable<int> sequence) {
            foreach (int x in sequence)
                if ((x % 2) == 0)
                    yield return x;
        }

        public static void Main(string[] args) {
            using (var enumerator = "beer".GetEnumerator()) {
                while (enumerator.MoveNext()) {
                    var element = enumerator.Current;
                    Console.WriteLine(element);
                }
            }

            foreach (int fib in Fibs(6))
                Console.Write(fib + " ");
            Console.WriteLine();

            foreach (string s in Foo(true))
                Console.WriteLine(s);

            foreach (int fib in EvenNumbersOnly(Fibs(10)))
                Console.WriteLine(fib);
        }
    }
}
