﻿using System;

namespace delegates {
    class MainClass {
        static int Square(int x) {
            return x * x;
        }

        delegate int Transformer(int x);

        public static void Main(string[] args) {
            Transformer t = Square;
            int result = t(3);
            Console.WriteLine(result);
        }
    }
}
