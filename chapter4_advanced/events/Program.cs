﻿using System;

namespace events {
    public class PriceChangedEventArgs : EventArgs {
        public readonly decimal LastPrice;
        public readonly decimal NewPrice;

        public PriceChangedEventArgs(decimal lastPrice, decimal newPrice) {
            LastPrice = lastPrice;
            NewPrice = newPrice;
        }
    }

    public class Stock {
        string symbol;
        decimal price;

        public Stock(string symbol) {
            this.symbol = symbol;
        }

        public event EventHandler<PriceChangedEventArgs> PriceChanged;

        protected virtual void OnPriceChanged(PriceChangedEventArgs e) {
            if (PriceChanged != null)
                PriceChanged(this, e);
        }

        public decimal Price { 
            get { return Price; }
            set {
                if (price == value) return;
                decimal oldPrice = price;
                price = value;
                OnPriceChanged(new PriceChangedEventArgs(oldPrice, price));
            }
        }
    }

    class MainClass {
        static void Stock_PriceChanged(object sender, events.PriceChangedEventArgs e) {
            if ((e.NewPrice - e.LastPrice) / e.LastPrice > 0.1M)
                Console.WriteLine("Alert, 10% stock price increase!");
        }

        public static void Main(string[] args) {
            Stock stock = new Stock("THPW");
            stock.Price = 27.10M;

            // Register with the PriceChanged event
            stock.PriceChanged += Stock_PriceChanged;

            stock.Price = 77.10M;
        }
    }
}
