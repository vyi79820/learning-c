﻿using System;

namespace Utils {
    public static class StringHelper {
        public static bool IsCapitalized(this string s) {
            if (string.IsNullOrEmpty(s)) return false;
            return char.IsUpper(s[0]);
        }
    }
}

namespace extesnsion_methods {
    using Utils;

    class MainClass {
        public static void Main(string[] args) {
            Console.WriteLine("Hello World!".IsCapitalized());
        }
    }
}
