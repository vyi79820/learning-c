﻿using System;
using System.IO;

namespace try_statement {
    class MainClass {
        static int Calc(int x) {
            return 10 / x;
        }

        static void Display(string name) {
            if (name == null)
                throw new ArgumentNullException("name");

            Console.WriteLine(name);
        } 

        public static void Main(string[] args) {
            try {
                int y = Calc(0);
                Console.WriteLine(y);
            } catch (DivideByZeroException) {
                Console.WriteLine("x cannot be zero");
            }

            try {
                byte b = byte.Parse(args[1]);
                Console.WriteLine(b);
            } catch (IndexOutOfRangeException) {
                Console.WriteLine("Please provide at least one argument");
            } catch (FormatException) {
                Console.WriteLine("That's not a number!");
            } catch (OverflowException) {
                Console.WriteLine("You've given me more than a byte!");
            } finally { 
                Console.WriteLine("program completed");
            }

            using (Stream sw = File.OpenWrite("test.tripinwithlsd")) {
                byte[] buffer = new byte[] {
                    (byte)'y', (byte)'e', (byte)'l',  (byte)'l', (byte)'o'
                };

                sw.Write(buffer, 0, buffer.Length);
            }

            try {
                Display(null);
            } catch (ArgumentException ex) {
                Console.WriteLine("error: "+ ex.Message);
            }
        }
    }
}
