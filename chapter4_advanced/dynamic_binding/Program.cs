﻿using System;
using System.Dynamic;
using System.Runtime.CompilerServices;

namespace dynamic_binding {
    public class Duck : DynamicObject { 
        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result) {
            Console.WriteLine(binder.Name + " method was called");
            result = null;
            return true;
        }
    }

    class MainClass {
        public static void Foo(
        [CallerMemberName] string memberName = null,
        [CallerFilePath] string filePath = null,
        [CallerLineNumber] int lineNumber = 0) {

            Console.WriteLine(memberName);
            Console.WriteLine(filePath);
            Console.WriteLine(lineNumber);
        }

        public static void Main(string[] args) {
            dynamic d = new Duck();
            d.Quak();
            d.Waddle();

            Foo();
        }
    }
}
