﻿using System;

namespace lambda {
    class MainClass {
        delegate int Transformer(int i);

        static Func<int> Natural() {
            int seed = 0;
            return () => seed++;
        }

        static Func<int> Naturall() {
            return () => {
                int seed = 0;
                return seed++;
            };
        }

        public static void Main(string[] args) {
            Transformer sqr = x => x * x;
            Console.WriteLine(sqr(3));

            int factor = 2;
            Func<int, int> multiplaier = n => n * factor;
            Console.WriteLine(multiplaier(3));

            factor = 10;
            Console.WriteLine(multiplaier(3));

            int seed = 0;
            Func<int> natural = () => seed++;
            Console.WriteLine(natural());
            Console.WriteLine(natural());
            Console.WriteLine(seed);

            Func<int> naturall = Natural();
            Console.WriteLine(naturall());
            Console.WriteLine(naturall());

            Func<int> naturalll = Naturall();
            Console.WriteLine(naturalll());
            Console.WriteLine(naturalll());

            Action[] actions = new Action[3];

            for (int i = 0; i < 3; i++)
                actions[i] = () => Console.Write(i);

            foreach (Action a in actions)
                a();

            Console.WriteLine();

            Transformer tr = (int x) => { return x * x; };
            Console.WriteLine(tr(3));
        }
    }
}
