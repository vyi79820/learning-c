﻿using System;

namespace nullable {
    class MainClass {
        public static void Main(string[] args) {
            string s = null;
            int? i = null;

            Console.WriteLine(s == null);
            Console.WriteLine(i == null);

            Nullable<int> ii = new Nullable<int>();
            Console.WriteLine(!ii.HasValue);

            object o = "string";
            int? x = o as int?;
            Console.WriteLine(x.HasValue);

            int? xx = 5;
            int? yy = 10;
            bool b = xx < yy;
            Console.WriteLine(b);

            bool bb = (x.HasValue && yy.HasValue) ? (x.Value < yy.Value) : false;
            Console.WriteLine(bb);

            int? c = (x.HasValue && yy.HasValue)
                     ? (int?)(x.Value + yy.Value)
                     : null;
            Console.WriteLine(c);

            int? xxx = null;
            int yyy = xxx ?? 5;
            int? aaa = null, bbb = 1, ccc = 2;
            Console.WriteLine(aaa ?? bbb ?? ccc);
        }
    }
}
