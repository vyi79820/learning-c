﻿using System;
using System.Linq;
using System.Data.Linq;
using System.Data.Entity;

namespace ending {
    [EdmEntityType(NamespaceName = "NutshellModel", Name = "Customer")]
    public partial class Customer {
        [EdmScalarPropertyAttribute(EntityKeyProperty = true, IsNullable = false)]
        public int ID { get; set; }
        [EdmScalarProperty(EntityKeyProperty = false, IsNullable = false)]
        public string Name { get; set; }
    }

    class NutshellContext : ObjectContext { 
        public Table<Customer> Customers { 
            get { return GetTable<Customer>(); }
        }
    }

    class MainClass {
        public static void Main(string[] args) {
            var context = new NutshellContext("connection string");

            Customer cust1 = context.Customers.OrderBy(c => c.Name).First();
            foreach (Purchase p in cust1.Purchases)
                Console.WriteLine(p.Price);
        }
    }
}
