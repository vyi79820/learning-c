﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace end {
    class MainClass {
        public static void Main(string[] args) {
            string[] files = Directory.GetFiles("/tmp/");

            var queryy = files.GroupBy(file => Path.GetExtension(file));

            foreach (IGrouping<string, string> grouping in queryy) {
                Console.WriteLine("Extension: " + grouping.Key);
                foreach (string filename in grouping)
                    Console.WriteLine("   - " + filename);
            }

            int[][] numbers = {
                new int[] { 1, 2, 3 },
                new int[] { 4, 5, 6 },
                null
            };

            IEnumerable<int> flat = numbers
                .SelectMany(innerArray => innerArray ?? Enumerable.Empty<int>());
            foreach (int i in flat)
                Console.Write(i + " ");
            Console.WriteLine();

            foreach (int i in Enumerable.Range(5, 3))
                Console.Write(i + " ");
            Console.WriteLine();

            foreach (int i in Enumerable.Repeat(5, 3))
                Console.Write(i + " ");
            Console.WriteLine();
        }
    }
}
