﻿using System;

namespace structs_modifiers_interfaces_enums {
    public struct Point {
        int x, y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public interface IEnumerator {
        bool MoveNext();
        object Current { get; }
        void Reset();
    }

    public interface I1 {
        void Foo();
    }

    public interface I2 {
        int Foo();
    }

    public interface IUndoable {
        void Undo();
    }

    internal class Countdown : IEnumerator {
        int count = 11;
        public bool MoveNext() {
            return count-- > 0;
        }

        public object Current { 
            get { return count; }
        }

        public void Reset() {
            throw new NotSupportedException();
        }
    }

    public class Widget : I1, I2 { 
        public void Foo() {
            Console.WriteLine("Widget's implementation of I1.Foo");
        }

        int I2.Foo() { 
            Console.WriteLine("Widget's implementation of I2.Foo");
            return 42;
        }
    }

    public class TextBox : IUndoable {
        public void Undo() {
            Console.WriteLine("TextBox.Undo");
        }
    }

    public class RichTextBox : TextBox, IUndoable { 
        public new void Undo() {
            Console.WriteLine("RichTextBox.Undo");
        }
    }

    public enum BorderSide : byte {
        Left = 1,
        Right = 2,
        Top = 10,
        Bottom = 11
    }

    class MainClass {
        public static void Main(string[] args) {
            Point p1 = new Point();
            Point p2 = new Point(2, 2);

            IEnumerator e = new Countdown();
            while (e.MoveNext())
                Console.Write(e.Current);
            Console.WriteLine();

            Widget w = new Widget();
            w.Foo();
            ((I1)w).Foo();
            ((I2)w).Foo();

            RichTextBox r = new RichTextBox();
            r.Undo();
            ((IUndoable)r).Undo();
            ((TextBox)r).Undo();

            BorderSide leftRight = BorderSide.Left | BorderSide.Right;

            if ((leftRight & BorderSide.Left) != 0)
                Console.WriteLine("Includes Left");

            string formatted = leftRight.ToString();

            BorderSide s = BorderSide.Left;
            s |= BorderSide.Right;

            Console.WriteLine(s == leftRight);

            s ^= BorderSide.Right;
            Console.WriteLine(s);
        }
    }
}
