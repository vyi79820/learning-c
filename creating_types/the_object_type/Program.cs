﻿using System;
using System.Drawing;

namespace the_object_type {
    class MainClass {
        public static void Main(string[] args) {
            object obj = 9;
            long x = (int)obj;

            Console.WriteLine(x);

            int i = 3;
            object o = i;
            i = 5;

            Console.WriteLine(o);

            Point p = new Point();
            Console.WriteLine(p.GetType().Name);
            Console.WriteLine(typeof(Point).Name);
            Console.WriteLine(p.GetType() == typeof(Point));
            Console.WriteLine(p.X.GetType().Name);
            Console.WriteLine(p.Y.GetType().FullName);
            Console.WriteLine(typeof(Point));
        }
    }
}
