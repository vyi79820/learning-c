﻿using System;

namespace creating_types {
    public class math {
        public int num1, num2;
        public bool two = false;

        public static int add(int a, int b) {
            return a + b;
        }

        public static float add(float a, float b) {
            return a + b;
        }

        public math() { }
        public math(int num) {
            num1 = num;
            two = false;
        }
        public math(int num, int num2) : this(num) {
            this.num2 = num2;
            two = true;
        }

        public int calc() {
            if (two == false)
                return num1 * 2;
            else return num1 + num2;
        }
    }

    public class Foo {
        private decimal x;
        public decimal X {
            get {
                return x;
            }

            set {
                x += value;
            }
        }

        string[] words = "The quick brown fox".Split();

        public string this[int i] { 
            get {
                return words[i];
            }

            set {
                words[i] = value;
            }
        }
    }

    class MainClass {
        private static readonly string _program_name = "creating_types",
                                       _program_creator = "no one";
        private const int i1 = 2,
                          i2 = 3;

        private const float f1 = 5.2f,
                            f2 = 333.33f;

        public static void Main(string[] args) {
            Console.WriteLine(_program_creator + " created program " + _program_name);
            Console.WriteLine(i1 + "\t+ " + i2 + "\t\t= " + math.add(i1, i2));
            Console.WriteLine(f1 + "\t+ " + f2 + "\t= " + math.add(f1, f2));

            math m1 = new math(1),
                 m2 = new math(1, 2);

            Console.WriteLine(m1.calc());
            Console.WriteLine(m2.calc());

            m2 = new math { num1 = 1, two = false };

            Console.WriteLine(m2.calc());

            Foo f = new Foo();
            f.X = 8;
            f.X = 8;
            Console.WriteLine(f.X);
            Console.WriteLine(f[0]);
        }
    }
}
