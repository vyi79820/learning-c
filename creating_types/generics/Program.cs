﻿using System;

namespace generics {
    public class Stack<T> {
        int position;
        T[] data = new T[100];

        public void Push(T obj) {
            data[position++] = obj;
        }

        public T Pop() {
            return data[--position];
        }
    }

    class MainClass {
        public static void Main(string[] args) {
            Stack<int> stack = new Stack<int>();
            stack.Push(10);
            stack.Push(5);

            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
        }
    }
}
