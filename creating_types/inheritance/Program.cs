﻿using System;

namespace inheritance {
    public class Asset {
        public string Name;
        public virtual decimal Liability {
            get {
                return 1;
            }
        }
    }

    public class House : Asset {
        public decimal People;
        public override decimal Liability {
            get {
                return People;
            }
        }
    }

    public class House2 : Asset {
        public decimal People;
        public override decimal Liability {
            get {
                return base.Liability + People;
            }
        }
    }

    class MainClass {
        public static void Main(string[] args) {
            House mansion = new House { Name = "marnieMansion", People = 6 };
            Asset a = mansion;
            Console.WriteLine(mansion.Liability);
            Console.WriteLine(a.Liability);

            House2 mansion2 = new House2 { Name = "homeMansion", People = 2 };
            Console.WriteLine(mansion2.Liability);
        }
    }
}
