﻿using System;

namespace dates_and_times {
    class MainClass {
        public static void Main(string[] args) {
            // TimeSpan
            Console.WriteLine("\n------ TimeSpan ------");

            Console.WriteLine(new TimeSpan(2, 30, 0));
            Console.WriteLine(TimeSpan.FromHours(2.5));
            Console.WriteLine(TimeSpan.FromHours(-2.5));

            TimeSpan nearlyTenDays = TimeSpan.FromDays(10) - TimeSpan.FromSeconds(1);
            Console.WriteLine(nearlyTenDays.Days);
            Console.WriteLine(nearlyTenDays.Hours);
            Console.WriteLine(nearlyTenDays.Minutes);
            Console.WriteLine(nearlyTenDays.Seconds);
            Console.WriteLine(nearlyTenDays.Milliseconds);

            Console.WriteLine(nearlyTenDays.TotalDays);
            Console.WriteLine(nearlyTenDays.TotalHours);
            Console.WriteLine(nearlyTenDays.TotalMinutes);
            Console.WriteLine(nearlyTenDays.TotalSeconds);
            Console.WriteLine(nearlyTenDays.TotalMilliseconds);

            // DateTime
            Console.WriteLine("\n------ DateTime ------");

            DateTime d = new DateTime(5767, 1, 1, new System.Globalization.HebrewCalendar());
            Console.WriteLine(d);

            Console.WriteLine(DateTime.Now);
            Console.WriteLine(DateTimeOffset.Now);
            Console.WriteLine(DateTime.Today);
            Console.WriteLine(DateTime.UtcNow);
            Console.WriteLine(DateTimeOffset.UtcNow);

            DateTime dt = new DateTime(2000, 2, 3, 10, 20, 30);

            Console.WriteLine(dt.Year);
            Console.WriteLine(dt.Month);
            Console.WriteLine(dt.Day);
            Console.WriteLine(dt.DayOfWeek);
            Console.WriteLine(dt.DayOfYear);

            Console.WriteLine(dt.Hour);
            Console.WriteLine(dt.Minute);
            Console.WriteLine(dt.Second);
            Console.WriteLine(dt.Millisecond);
            Console.WriteLine(dt.Ticks);
            Console.WriteLine(dt.TimeOfDay);
        }
    }
}
