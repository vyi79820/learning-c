﻿using System;
using System.Globalization;
using System.Text;

namespace formating_parsing_to_conversion_mechanisms {
    public class WordyFormatProvider : IFormatProvider, ICustomFormatter {
        static readonly string[] _numberWords =
        "zero one two three four five six seven eight nine minus point".Split();
        IFormatProvider _parent;
        // Allows consumers to chain format providers
        public WordyFormatProvider() : this(CultureInfo.CurrentCulture) { }
        public WordyFormatProvider(IFormatProvider parent) {
            _parent = parent;
        }
        public object GetFormat(Type formatType) {
            if (formatType == typeof(ICustomFormatter)) return this;
            return null;
        }
        public string Format(string format, object arg, IFormatProvider prov) {
            // If it's not our format string, defer to the parent provider:
            if (arg == null || format != "W")
                return string.Format(_parent, "{0:" + format + "}", arg);
            
            StringBuilder result = new StringBuilder();
            string digitList = string.Format(CultureInfo.InvariantCulture,
            "{0}", arg);
            foreach (char digit in digitList) {
                int i = "0123456789-.".IndexOf(digit);
                if (i == -1) continue;
                if (result.Length > 0) result.Append(' ');
                result.Append(_numberWords[i]);
            }
            return result.ToString();
        }
    }

    class MainClass {
        public static void Main(string[] args) {
            Console.WriteLine("\n------ Parse ------");
            Console.WriteLine(double.Parse("1.234"));

            Console.WriteLine("\n------ Format Providers ------");
            NumberFormatInfo f = new NumberFormatInfo();
            f.CurrencySymbol = "$$";
            Console.WriteLine(3.ToString("C", f));

            Console.WriteLine(10.3.ToString("C", null));
            Console.WriteLine(10.3.ToString("C"));
            Console.WriteLine(10.3.ToString("F4"));

            Console.WriteLine("\n------ Format Providers and CultureInfo ------");
            CultureInfo uk = CultureInfo.GetCultureInfo("en-GB");
            Console.WriteLine(3.ToString("C", uk));

            DateTime dt = new DateTime(2000, 1, 2);
            CultureInfo iv = CultureInfo.InvariantCulture;
            Console.WriteLine(dt.ToString(iv));
            Console.WriteLine(dt.ToString("d", iv));

            Console.WriteLine("\n------ Composite formatting ------");
            string composite = "Credit={0:C}";
            Console.WriteLine(string.Format(composite, 500));
            Console.WriteLine("Credit={0:C}", 500);

            Console.WriteLine("\n------ IFormatProvider and ICustomFormatter ------");
            double n = -123.45;
            IFormatProvider fp = new WordyFormatProvider();
            Console.WriteLine(string.Format(fp, "{0:C} in words is {0:W}", n));

            Console.WriteLine("\n------ Dynamic conversions ------");
            Type targetType = typeof(int);
            object source = "42";
            object result = Convert.ChangeType(source, targetType);

            Console.WriteLine(result);
            Console.WriteLine(result.GetType());

            Console.WriteLine("\n------ BitConverter ------");
            foreach (byte b in BitConverter.GetBytes(3.5))
                Console.Write(b + " ");
            Console.WriteLine();
        }
    }
}
