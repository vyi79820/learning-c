﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Reflection;

namespace ending {
    public class Animal {
        public string Name;
        public int Popularity;

        public Animal(string name, int popularity) {
            Name = name;
            Popularity = popularity;
        }
    }

    public class AnimalCollection : Collection<Animal> { }

    public class Zoo { 
        public readonly AnimalCollection Animals = new AnimalCollection();
    }

    public class Customer { 
        public string LastName;
        public string FirstName;

        public Customer(string last, string first) { 
            LastName = last;
            FirstName = first;
        }
    }

    public class LastFirstEqComparer : EqualityComparer<Customer> {
        public override bool Equals(Customer x, Customer y) { 
            return x.LastName == y.LastName && x.FirstName == y.FirstName;
        }

        public override int GetHashCode(Customer obj) {
            return (obj.LastName + ";" + obj.FirstName).GetHashCode();
        }
    }

    class Wish { 
        public string Name;
        public int Priority;

        public Wish(string name, int priority) { 
            Name = name;
            Priority = priority;
        }
    }

    class PriorityComparer : Comparer<Wish> {
        public override int Compare(Wish x, Wish y) { 
            if (object.Equals(x, y)) return 0;
            return x.Priority.CompareTo(y.Priority);
        }
    }

    class SurnameComparer : Comparer<string> {
        string Normalize(string s) {
            s = s.Trim().ToUpper();
            if (s.StartsWith("MC")) s = "MAC" + s.Substring(2);
            return s;
        }

        public override int Compare(string x, string y) {
            return Normalize(x).CompareTo(Normalize(y));
        }
    }

    class MainClass {
        public static void Main(string[] args) {
            List<string> words = new List<string>();

            words.Add("melon");
            words.Add("avocado");
            words.AddRange(new[] { "banana", "plum" });
            words.Insert(0, "lemon");
            words.InsertRange(0, new[] { "peach", "nashi" });

            words.Remove("melon");
            words.RemoveAt(3);
            words.RemoveRange(0, 2);

            words.RemoveAll(s => s.StartsWith("n"));

            Console.WriteLine(words[0]);
            Console.WriteLine(words[words.Count - 1]);
            foreach (string s in words) Console.WriteLine(s);

            var tune = new LinkedList<string>();
            tune.AddFirst("do");
            tune.AddLast("so");

            tune.AddAfter(tune.First, "re");
            tune.AddAfter(tune.First.Next, "mi");
            tune.AddBefore(tune.Last, "fa");

            tune.RemoveFirst();
            tune.RemoveLast();

            LinkedListNode<string> miNode = tune.Find("mi");
            tune.Remove(miNode);

            tune.AddFirst(miNode);

            foreach (string s in tune) Console.WriteLine(s);

            var ss = new Stack<int>();
            ss.Push(1);
            ss.Push(2);
            ss.Push(3);
            Console.WriteLine(ss.Count);
            Console.WriteLine(ss.Peek());
            Console.WriteLine(ss.Pop());
            Console.WriteLine(ss.Pop());
            Console.WriteLine(ss.Pop());
            //Console.WriteLine(ss.Pop());


            var letters = new HashSet<char>("the quick brown fox");
            Console.WriteLine(letters.Contains('t'));
            Console.WriteLine(letters.Contains('j'));

            foreach (char c in letters) Console.Write(c);

            var d = new Dictionary<string, int>();
            d.Add("One", 1);
            d["Two"] = 2;
            d["Two"] = 22;
            d["Three"] = 3;

            Console.WriteLine(d["Two"]);
            Console.WriteLine(d.ContainsKey("One"));
            Console.WriteLine(d.ContainsValue(3));
            int val = 0;
            if (!d.TryGetValue("onE", out val))
                Console.WriteLine("No val");

            foreach (KeyValuePair<string, int> kv in d)
                Console.WriteLine(kv.Key + "; " + kv.Value);

            foreach (string s in d.Keys) Console.Write(s);
            Console.WriteLine();
            foreach (int i in d.Values) Console.Write(i);
            Console.WriteLine();

            var sorted = new SortedList<string, MethodInfo>();
            foreach (MethodInfo m in typeof(object).GetMethods())
                sorted[m.Name] = m;
            foreach (string name in sorted.Keys)
                Console.WriteLine(name);
            foreach (MethodInfo m in sorted.Values)
                Console.WriteLine(m.Name + " returns a " + m.ReturnType);

            Zoo zoo = new Zoo();
            zoo.Animals.Add(new Animal("Kangaroo", 10));
            zoo.Animals.Add(new Animal("Mr Sea Lion", 20));
            foreach (Animal a in zoo.Animals) Console.WriteLine(a.Name);

            Customer c1 = new Customer("Bloggs", "Joe");
            Customer c2 = new Customer("Bloggs", "Joe");

            Console.WriteLine(c1 == c2);
            Console.WriteLine(c1.Equals(c2));

            var dd = new Dictionary<Customer, string>();
            dd[c1] = "Joe";
            Console.WriteLine(dd.ContainsKey(c2));

            var wishLst = new List<Wish>();
            wishLst.Add(new Wish("Peace", 2));
            wishLst.Add(new Wish("Wealth", 3));
            wishLst.Add(new Wish("Love", 2));
            wishLst.Add(new Wish("3 more wishes", 1));

            wishLst.Sort(new PriorityComparer());
            foreach (Wish w in wishLst)
                Console.Write(w.Name + " | ");
            Console.WriteLine();

            var dic = new SortedDictionary<string, string>(new SurnameComparer());
            dic.Add("MacPhail", "second!");
            dic.Add("MacWilliam", "third!");
            dic.Add("McDonald", "first!");

            foreach (string s in dic.Values)
                Console.Write(s + " ");
            Console.WriteLine();

            int[] a1 = { 1, 2, 3 };
            int[] a2 = { 1, 2, 3 };
            IStructuralEquatable se1 = a1;
            Console.Write(a1.Equals(a2));
            Console.Write(se1.Equals(a2, EqualityComparer<int>.Default));
            Console.WriteLine();
        }
    }
}
