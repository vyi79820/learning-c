﻿using System;
using System.Numerics;
using System.Text;

namespace globalization_to_equality_comparison {
    class MainClass {
        enum Nut { Walnut, Hazelnut, Macadamia }
        enum Size { Small, Medium, Large }

        static void Display(Enum value) {
            Console.WriteLine(value.GetType().Name + "." + value.ToString());
        }

        static int GetIntegralValue(Enum anyEnum) {
            return (int)(object)anyEnum;
        }

        static decimal GetAnyIntegralValue(Enum anyEnum) {
            return Convert.ToDecimal(anyEnum);
        }

        static object GetBoxedIntegralValue(Enum anyEnum) {
            Type integralType = Enum.GetUnderlyingType(anyEnum.GetType());
            return Convert.ChangeType(anyEnum, integralType);
        }

        static string GetIntegralValueAsString(Enum anyEnum) {
            return anyEnum.ToString("D");
        }

        public struct Area : IEquatable<Area> {
            public readonly int Measure1;
            public readonly int Measure2;

            public Area(int m1, int m2) {
                Measure1 = Math.Min(m1, m2);
                Measure2 = Math.Max(m1, m2);
            }
            public override bool Equals(object other) {
                if (!(other is Area)) return false;
                return Equals((Area)other);
                // Calls method below
            }
            public bool Equals(Area other)
            // Implements IEquatable<Area>
            {
                return Measure1 == other.Measure1 && Measure2 == other.Measure2;
            }
            public override int GetHashCode() {
                return Measure2 * 31 + Measure1;
            }
            // 31 = some prime number
            public static bool operator ==(Area a1, Area a2) {
                return a1.Equals(a2);
            }
            public static bool operator !=(Area a1, Area a2) {
                return !a1.Equals(a2);
            }
        }

        [Flags]
        public enum BorderSides { Left = 1, Right = 2, Top = 4, Bottom = 8 }

        public static void Main(string[] args) {
            Console.WriteLine("\n------ BigInteger ------");
            BigInteger googol = BigInteger.Parse("1".PadRight(100, '0'));
            Console.WriteLine(googol.ToString());

            double g2 = (double)googol;
            BigInteger g3 = (BigInteger)g2;
            Console.WriteLine(g3);

            Console.WriteLine("\n------ Complex ------");
            var c1 = new Complex(2, 3.5);
            var c2 = new Complex(3, 0);

            Console.WriteLine(c1.Real);
            Console.WriteLine(c1.Imaginary);
            Console.WriteLine(c1.Phase);
            Console.WriteLine(c1.Magnitude);

            Complex c3 = Complex.FromPolarCoordinates(1.3, 5);
            Console.WriteLine(c1 + c2);
            Console.WriteLine(c1 * c2);

            Console.WriteLine("\n------ Random ------");
            Random r1 = new Random(1);
            Random r2 = new Random(1);
            Console.WriteLine(r1.Next(100) + ", " + r1.Next(100));
            Console.WriteLine(r2.Next(100) + ", " + r2.Next(100));

            Console.WriteLine("\n------ Enums ------");
            Display(Nut.Macadamia);
            Display(Size.Large);

            object result = GetBoxedIntegralValue(BorderSides.Top);
            Console.WriteLine(result);
            Console.WriteLine(result.GetType());

            object bs = Enum.ToObject(typeof(BorderSides), 3);
            Console.WriteLine(bs);

            Console.WriteLine("\n------ Enumerating Enum Values ------");
            foreach (Enum value in Enum.GetValues(typeof(BorderSides)))
                Console.WriteLine(value);

            Console.WriteLine("\n------ Tuples ------");
            Tuple<int, string> t = Tuple.Create(123, "Hello");
            Console.WriteLine(t.Item1 * 2);
            Console.WriteLine(t.Item2.ToUpper());

            object[] items = { 123, "Hello" };
            Console.WriteLine(((int)items[0]) * 2);
            Console.WriteLine(((string)items[1]).ToUpper());

            Console.WriteLine("\n------ Comparing Tuples ------");
            var t1 = Tuple.Create(123, "Hello");
            var t2 = Tuple.Create(123, "Hello");
            Console.WriteLine(t1 == t2);
            Console.WriteLine(t1.Equals(t2));

            Console.WriteLine("\n------ The Guid Struct ------");
            Guid g = Guid.NewGuid();
            Console.WriteLine(g.ToString());

            Guid g11 = new Guid("{0d57629c-7d6e-4847-97cb-9e2fc25083fe}");
            Guid g22 = new Guid("0d57629c7d6e484797cb9e2fc25083fe");
            Console.WriteLine(g11 == g22);

            Console.WriteLine("\n------ Equality Comparison ------");
            int x = 5, y = 5;
            Console.WriteLine(x == y);

            var dt1 = new DateTimeOffset(2010, 1, 1, 1, 1, 1, TimeSpan.FromHours(8));
            var dt2 = new DateTimeOffset(2010, 1, 1, 2, 1, 1, TimeSpan.FromHours(9));
            Console.WriteLine(dt1 == dt2);

            Uri uri1 = new Uri("http://www.linqpad.net");
            Uri uri2 = new Uri("http://www.linqpad.net");
            Console.WriteLine(uri1 == uri2);

            Console.WriteLine("\n------ The static object.Equals method ------");
            object xx = 3, yy = 3;
            Console.WriteLine(object.Equals(xx, yy));
            xx = null;
            Console.WriteLine(object.Equals(xx, yy));
            yy = null;
            Console.WriteLine(object.Equals(xx, yy));

            Console.WriteLine("\n------ When Equals and == are not equal ------");
            double xxx = double.NaN;
            Console.WriteLine(xxx == xxx);
            Console.WriteLine(xxx.Equals(xxx));

            var sb11 = new StringBuilder("foo");
            var sb22 = new StringBuilder("foo");
            Console.WriteLine(sb11 == sb22);
            Console.WriteLine(sb11.Equals(sb22));

            Area a1 = new Area(5, 10);
            Area a2 = new Area(10, 5);
            Console.WriteLine(a1.Equals(a2));
            Console.WriteLine(a1 == a2);
        }
    }
}
