﻿using System;
using System.Globalization;

namespace dates_and_time_zones {
    class MainClass {
        static string FormatTransitionTime(TimeZoneInfo.TransitionTime tt, bool endTime) {
            if (endTime && tt.IsFixedDateRule
                && tt.Day == 1 && tt.Month == 1
                && tt.TimeOfDay == DateTime.MinValue)
                return "-";

            string s;
            if (tt.IsFixedDateRule)
                s = tt.Day.ToString();
            else
                s = "The " +
                "first second third fourth last".Split()[tt.Week - 1] +
                " " + tt.DayOfWeek + " in";

            return s + " " + DateTimeFormatInfo.CurrentInfo.MonthNames[tt.Month - 1] + " at " + tt.TimeOfDay.TimeOfDay;
        }

        public static void Main(string[] args) {
            // DateTime and Time Zones
            Console.WriteLine("\n------ DateTime and Time Zones ------");

            DateTime dt1 = new DateTime(2000, 1, 1, 10, 20, 30, DateTimeKind.Local);
            DateTime dt2 = new DateTime(2000, 1, 1, 10, 20, 30, DateTimeKind.Utc);

            Console.WriteLine(dt1 == dt2);
            DateTime local = DateTime.Now;
            DateTime utc = local.ToUniversalTime();
            Console.WriteLine(local == utc);

            DateTime d = new DateTime(2000, 12, 12);
            DateTime utcc = DateTime.SpecifyKind(d, DateTimeKind.Utc);
            Console.WriteLine(utcc);

            // DateTimeOffset and Time Zones
            Console.WriteLine("\n------ DateTimeOffset and Time Zones ------");

            DateTimeOffset locall = DateTimeOffset.Now;
            DateTimeOffset utccc = locall.ToUniversalTime();

            Console.WriteLine(locall.Offset);
            Console.WriteLine(utccc.Offset);
            Console.WriteLine(locall == utccc);
            Console.WriteLine(locall.EqualsExact(utccc));

            // TimeZone
            Console.WriteLine("\n------ TimeZone ------");

            TimeZone zone = TimeZone.CurrentTimeZone;
            Console.WriteLine(zone.StandardName);
            Console.WriteLine(zone.DaylightName);

            DateTime dt11 = new DateTime(2008, 1, 1);
            DateTime dt22 = new DateTime(2008, 6, 1);
            Console.WriteLine(zone.IsDaylightSavingTime(dt11));
            Console.WriteLine(zone.IsDaylightSavingTime(dt22));
            Console.WriteLine(zone.GetUtcOffset(dt11));
            Console.WriteLine(zone.GetUtcOffset(dt22));

            DaylightTime day = zone.GetDaylightChanges(2008);
            Console.WriteLine(day.Start);
            Console.WriteLine(day.End);
            Console.WriteLine(day.Delta);

            // TimeZoneInfo
            Console.WriteLine("\n------ TimeZoneInfo ------");

            TimeZoneInfo zonee = TimeZoneInfo.Local;
            Console.WriteLine(zonee.StandardName);
            Console.WriteLine(zonee.DaylightName);

            TimeZoneInfo wa = TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
            Console.WriteLine(wa.Id);
            Console.WriteLine(wa.DisplayName);
            Console.WriteLine(wa.BaseUtcOffset);
            Console.WriteLine(wa.SupportsDaylightSavingTime);

            Console.WriteLine();
            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                Console.WriteLine(z.Id);
            Console.WriteLine();

            foreach (TimeZoneInfo.AdjustmentRule rule in wa.GetAdjustmentRules())
                Console.WriteLine("Rule: applies from " + rule.DateStart + " to " + rule.DateEnd);

            foreach (TimeZoneInfo.AdjustmentRule rule in wa.GetAdjustmentRules()) {
                Console.WriteLine("Rule: applies from " + rule.DateStart + " to " + rule.DateEnd);
                Console.WriteLine(" Delta: " + rule.DaylightDelta);
                Console.WriteLine(" Start: " + FormatTransitionTime(rule.DaylightTransitionStart, false));
                Console.WriteLine(" End: " + FormatTransitionTime(rule.DaylightTransitionEnd, true));
                Console.WriteLine();
            }

            // Daylight Saving Time and DateTime
            Console.WriteLine("\n------ Daylight Saving Time and DateTime ------");

            DaylightTime changes = TimeZone.CurrentTimeZone.GetDaylightChanges(2010);
            TimeSpan halfDelta = new TimeSpan(changes.Delta.Ticks / 2);
            DateTime utc1 = changes.End.ToUniversalTime() - halfDelta;
            DateTime utc2 = utc1 - changes.Delta;

            DateTime loc1 = utc1.ToLocalTime();
            DateTime loc2 = utc2.ToLocalTime();
            Console.WriteLine(loc1);
            Console.WriteLine(loc2);
            Console.WriteLine(loc1 == loc2);

            Console.Write(DateTime.Now.IsDaylightSavingTime());
            Console.Write(DateTime.UtcNow.IsDaylightSavingTime());

            Console.Write(loc1.ToString("o"));
            Console.Write(loc2.ToString("o"));
        }
    }
}
