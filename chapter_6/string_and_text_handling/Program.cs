﻿using System;
using System.Text;

namespace string_and_text_handling {
    class MainClass {
        public static void Main(string[] args) {
            // Constructing strings
            Console.WriteLine("\n------ Constructing strings ------");

            string s1 = "Hello";
            string s2 = "First Line\r\nSecond Line";
            string s3 = @"\\server\\fileshare\\helloworld.xs";

            Console.WriteLine(s1);
            Console.WriteLine(s2);
            Console.WriteLine(s3);
            Console.WriteLine(new string('*', 10));

            char[] ca = "hello".ToCharArray();
            string s = new string(ca);
            Console.WriteLine(s);


            // Null and empty strings
            Console.WriteLine("\n------ Null and empty strings ------");

            string empty = "";
            Console.WriteLine(empty == "");
            Console.WriteLine(empty == string.Empty);
            Console.WriteLine(empty.Length == 0);

            string nullstr = null;
            Console.WriteLine(nullstr == null);
            Console.WriteLine(nullstr == "");
            //Console.WriteLine(nullstr.Length == 0); // NullReferenceException

            // Searching within strings
            Console.WriteLine("\n------ Searching within strings ------");

            Console.WriteLine("quick brows fox".Contains("quick"));
            Console.WriteLine("quick brows fox".Contains("s f"));
            Console.WriteLine("abcde".IndexOf("de"));
            Console.WriteLine("abcdef".IndexOf("Bc", StringComparison.CurrentCultureIgnoreCase));
            Console.WriteLine("ab,cd ef".IndexOfAny(new char[] { ' ', ',' }));
            Console.WriteLine("pas5w0rd".IndexOfAny("0123456789".ToCharArray()));


            // Manipulating strings
            Console.WriteLine("\n------ Manipulating strings ------");

            string left3 = "12345".Substring(0, 3);
            string mid3 = "12345".Substring(1, 3);
            string end3 = "12345".Substring(2);

            Console.WriteLine(left3);
            Console.WriteLine(mid3);
            Console.WriteLine(end3);

            string ss1 = "helloworld".Insert(5, ", ");
            string ss2 = ss1.Remove(5, 2);

            Console.WriteLine(ss1);
            Console.WriteLine(ss2);

            Console.WriteLine("12345".PadLeft(9, '*'));
            Console.WriteLine("12345".PadLeft(9));

            // Splitting and joining strings
            Console.WriteLine("\n------ Splitting and joining strings ------");

            string[] words = "the quick brown fox".Split();

            foreach (string word in words)
                Console.Write(word + "|");
            Console.WriteLine();

            string together = string.Join(" ", words);
            Console.WriteLine(together);

            string composite = "{0} mens were diving with {1} {2} colored dogs";
            Console.WriteLine(string.Format(composite, 2, 2, "yellow"));

            composite = "Name={0,-20} Credit Limit={1,15:C}";
            Console.WriteLine(string.Format(composite, "Mary", 500));
            Console.WriteLine(string.Format(composite, "Elizabeth", 20000));
            Console.WriteLine("Name=" + "Mary".PadRight(20) + " Credit Limit=" + 500.ToString("C").PadLeft(15));

            // Comparing Strings
            Console.WriteLine("\n------ Comparing Strings ------");

            Console.WriteLine(string.Equals("foo", "FOO", StringComparison.OrdinalIgnoreCase));
            Console.WriteLine("ṻ" == "ǖ");
            Console.WriteLine(string.Equals("ṻ", "ǖ", StringComparison.CurrentCulture));

            Console.WriteLine("Boston".CompareTo("Austin"));
            Console.WriteLine("Boston".CompareTo("Boston"));
            Console.WriteLine("Boston".CompareTo("Chicago"));
            Console.WriteLine("ṻ".CompareTo("ǖ"));
            Console.WriteLine("foo".CompareTo("FOO"));

            // StringBuilder
            Console.WriteLine("\n------ StringBuilder ------");

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 50; i++)
                sb.Append(i + ",");
            Console.WriteLine(sb.ToString());

            byte[] utf8Bytes = System.Text.Encoding.UTF8.GetBytes("0123456789");
            byte[] utf16Bytes = System.Text.Encoding.Unicode.GetBytes("0123456789");
            byte[] utf32Bytes = System.Text.Encoding.UTF32.GetBytes("0123456789");
            Console.WriteLine(utf8Bytes.Length);
            Console.WriteLine(utf16Bytes.Length);
            Console.WriteLine(utf32Bytes.Length);

            string original1 = System.Text.Encoding.UTF8.GetString(utf8Bytes);
            string original2 = System.Text.Encoding.Unicode.GetString(utf16Bytes);
            string original3 = System.Text.Encoding.UTF32.GetString(utf32Bytes);
            Console.WriteLine(original1);
            Console.WriteLine(original2);
            Console.WriteLine(original3);
        }
    }
}
