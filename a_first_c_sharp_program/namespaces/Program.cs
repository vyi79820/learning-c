﻿using System;

namespace neko {
    public class janku {
        string name = "";
        public janku(string name) {
            this.name = name;
        }

        public void print() {
            Console.WriteLine(name);
        }
    }
}

namespace namespaces {
    class MainClass {
        public static void Main(string[] args) {
            neko.janku j1 = new neko.janku("citten");
            neko.janku j2 = new neko.janku("bunny");

            j1.print();
            j2.print();
        }
    }
}
