﻿using System;

namespace statements {
    class MainClass {
        private static void TellMeWhatICanDo(int age) {
            if (age >= 35) Console.WriteLine("You can be president!");
            else if (age >= 21) Console.WriteLine("You can drink!");
            else if (age >= 18) Console.WriteLine("You can vote!");
            else Console.WriteLine("You can wait!");
        }

        private static void ShowCard(int cardNumber) {
            switch (cardNumber) {
                case 13:
                    Console.WriteLine("King");
                    break;
                case 12:
                    Console.WriteLine("Queen");
                    break;
                case 11:
                    Console.WriteLine("Jack");
                    break;
                case -1:
                    goto case 12;
                default:
                    Console.WriteLine(cardNumber);
                    break;
            }
        }

        private static int readNumber(string str) { 
            Console.Write(str + ": ");

            while (true) {
                try {
                    int i = Convert.ToInt32(Console.ReadLine());
                    return i;
                } catch (Exception) { }
            }
        }

        public static void Main(string[] args) {
            if (true)
                if (false)
                    Console.WriteLine();
                else Console.WriteLine("executes");

            if (true) {
                if (false)
                    Console.WriteLine();
            } else Console.WriteLine("does not execute");

            int age = readNumber("Tell me your age");
            TellMeWhatICanDo(age);

            int card = readNumber("Tell me your card number");
            ShowCard(card);

            for (int i = 0, prevFib = 1, curFib = 1; i < 10; i++) {
                Console.WriteLine(prevFib);
                int newFib = prevFib + curFib;
                prevFib = curFib; curFib = newFib;
            }

            foreach (char c in "beer")
                Console.WriteLine(c);

            for (int i = 0; i < 10; i++) {
                if ((i % 2) == 2)
                    continue;

                Console.Write(i + " ");
            } Console.WriteLine();

            int j = 0;
        startLoop:
            if (j <= 5) {
                Console.Write(j + " ");
                j++;
                goto startLoop;
            } Console.WriteLine();
        }
    }
}
