﻿using System;

/*
class Test {
    static void Main() {
        int x = 12 * 30;
        Console.WriteLine(x);
    }
}
*/

class Test {
    static int FeetToInches(int feet) {
        int inches = feet * 12;
        return inches;
    }

    static void Main() {
        Console.WriteLine(FeetToInches(30));    // 360
        Console.WriteLine(FeetToInches(100));    // 1200
    }
}