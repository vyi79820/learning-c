﻿using System;
using System.Drawing;

namespace typ_basics {
    public class UnitConverter {
        int ratio;
        public UnitConverter(int unitRatio) { ratio = unitRatio; }
        public int Convert(int unit) { return unit * ratio; }
    }

    public class Panda {
        public string Name;
        public static int Population;

        public Panda(string n) {
            Name = n;
            Population = Population + 1;
        }
    }

    class MainClass {
        public static void Main(string[] args) {
            string message = "Hello world";
            string upperMessage = message.ToUpper();
            Console.WriteLine(upperMessage);

            int x = 2012;
            message = message + x.ToString();
            Console.WriteLine(message);

            bool simpleVar = false;
            if (simpleVar)
                Console.WriteLine("This will not print");

            x = 5000;
            bool lessThanAMile = x < 5280;
            if (lessThanAMile)
                Console.WriteLine("This will print");

            UnitConverter feetToInchesConverter = new UnitConverter(12);
            UnitConverter milesToFeetConverter = new UnitConverter(5280);

            Console.WriteLine(feetToInchesConverter.Convert(30));
            Console.WriteLine(feetToInchesConverter.Convert(100));
            Console.WriteLine(feetToInchesConverter.Convert(
                milesToFeetConverter.Convert(1)
            ));

            Panda p1 = new Panda("Pan Dee");
            Panda p2 = new Panda("Pan Dah");

            Console.WriteLine(p1.Name);
            Console.WriteLine(p2.Name);
            Console.WriteLine(Panda.Population);

            Point pp1 = new Point();
            pp1.X = 7;

            Point pp2 = pp1;

            Console.WriteLine(pp1.X);
            Console.WriteLine(pp2.X);

            pp1.X = 9;

            Console.WriteLine(pp1.X);
            Console.WriteLine(pp2.X);
        }
    }
}
