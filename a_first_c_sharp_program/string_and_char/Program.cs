﻿using System;
using System.Drawing;
using System.Text;

class MainClass {
    public static int Factorial(int x) {
        if (x == 0) return 1;
        return x * Factorial(x - 1);
    }

    public static void Swap(ref string a, ref string b) {
        string temp = a;
        a = b;
        b = temp;
    }

    public static void Split(string name, out string firstNames, out string lastName) {
        int i = name.LastIndexOf(' ');
        firstNames = name.Substring(0, i);
        lastName = name.Substring(i + 1);
    }

    public static int xx;
    public static void Foo(out int y) {
        Console.WriteLine(xx);
        y = 1;
        Console.WriteLine(xx);
    }

    public static int Sum(params int[] ints) {
        int sum = 0;
        for (int i = 0; i < ints.Length; i++)
            sum += ints[i];
        return sum;
    }

    public static void foo(int x, int y) {
        Console.WriteLine(x + ", " + y);
    }

    public static void Main(string[] args) {
        string escaped = "First Line\r\nSecond Line";
        string verbatim = @"First Line
        Second Line";

        Console.WriteLine(escaped == verbatim);

        char[] vowels = new char[5];
        vowels[0] = 'a';
        vowels[1] = 'e';
        vowels[2] = 'i';
        vowels[3] = 'o';
        vowels[4] = 'u';

        Console.WriteLine(vowels[1]);

        for (int i = 0; i < vowels.Length; i++)
            Console.Write(vowels[i]);
        Console.WriteLine();

        Console.WriteLine(Factorial(4));

        StringBuilder ref1 = new StringBuilder("object1");
        Console.WriteLine(ref1);

        StringBuilder ref2 = new StringBuilder("object2");
        StringBuilder ref3 = ref2;
        Console.WriteLine(ref3);

        string x = "Pren";
        string y = "Teller";

        Swap(ref x, ref y);

        Console.WriteLine(x);
        Console.WriteLine(y);

        string a, b;
        Split("Stevie Ray Vaughan", out a, out b);

        Console.WriteLine(a);
        Console.WriteLine(b);

        Foo(out xx);

        int total = Sum(1, 2, 3, 4);
        Console.WriteLine(total);

        foo(y: 12, x: 22);

        int aa = 0;
        foo(y: ++aa, x: --aa);
    }
}
