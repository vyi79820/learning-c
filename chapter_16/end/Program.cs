﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace end {
    class WebServer {
        HttpListener _listener;
        string _baseFolder;

        public WebServer(string uriPrefix, string baseFolder) {
            _listener = new HttpListener();
            _listener.Prefixes.Add(uriPrefix);
            _baseFolder = baseFolder;
        }

        public async void Start() {
            _listener.Start();

            while (true)
                try {
                    var context = await _listener.GetContextAsync();
                    Task.Run(() => ProcessRequestAsync(context));
                }
                catch (HttpListenerException) { break; }
                catch (InvalidOperationException) { break; }
        }

        public void Stop() { _listener.Stop(); }

        async void ProcessRequestAsync(HttpListenerContext context) {
            try {
                string filename = Path.GetFileName(context.Request.RawUrl);
                string path = Path.Combine(_baseFolder, filename);
                byte[] msg;
                if (!File.Exists(path)) {
                    Console.WriteLine("Resource not found: " + path);
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    msg = Encoding.UTF8.GetBytes("Sorry, that page does not exist");
                } else {
                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                    msg = File.ReadAllBytes(path);
                }
                context.Response.ContentLength64 = msg.Length;
                using (Stream s = context.Response.OutputStream)
                    await s.WriteAsync(msg, 0, msg.Length);
            } catch (Exception ex) { Console.WriteLine("Request error: " + ex); }
        }
    }

    class MainClass {
        async static void ListenAsync() { 
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:51111/MyApp/");
            listener.Start();

            HttpListenerContext context = await listener.GetContextAsync();

            string msg = "You asked for: " + context.Request.RawUrl;
            context.Response.ContentLength64 = Encoding.UTF8.GetByteCount(msg);
            context.Response.StatusCode = (int)HttpStatusCode.OK;

            using (Stream s = context.Response.OutputStream)
            using (StreamWriter writer = new StreamWriter(s))
                await writer.WriteAsync(msg);
            
            listener.Stop();
        }

        public static void Main(string[] args) {
            /*
            ListenAsync();
            WebClient wc = new WebClient();
            Console.WriteLine(wc.DownloadString("http://localhost:51111/MyApp/Request.txt"));
            */
            /*
            var server = new WebServer("http://localhost:51111/", @"d:\webroot");
            try {
                server.Start();
                Console.WriteLine("Server running... press Enter to stop");
                Console.ReadLine();
            } finally { server.Stop(); }
            */


            /*
            MailMessage mm = new MailMessage();
            mm.Sender = new MailAddress("kay@domain.com", "Kay");
            mm.From = new MailAddress("kay@domain.com", "Kay");
            mm.To.Add(new MailAddress("koq42031@dsiay.com", "la"));
            mm.Subject = "Hello!";
            mm.Body = "Hi there. Here's the photo!";
            mm.IsBodyHtml = false;
            mm.Priority = MailPriority.High;
            client.Send(mm);
            */
        }
    }
}
